Django==2.2.3
psycopg2-binary==2.8.4
djangorestframework==3.9.4
dj-database-url==0.5.0
django-extensions==2.1.9
django-filter==2.1.0
djangorestframework-camel-case==1.0.3
ipython==7.6.1
gunicorn==19.9.0
Werkzeug==0.15.4
pyinotify==0.9.6
whitenoise==4.1.3
sentry-sdk==0.10.2
celery==4.3.0
django-ordered-model==3.3.0
drf-yasg==1.17.0
python-dateutil==2.8.1
django-celery-beat==2.0.0
