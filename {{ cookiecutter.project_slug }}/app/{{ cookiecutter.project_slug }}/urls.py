from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from rest_framework import routers
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from {{ cookiecutter.project_slug }}.api import ProfileView


schema_view = get_schema_view(
    openapi.Info(
        title="{{ cookiecutter.project_slug }}Api",
        default_version='v1',
        description="Api for {{ cookiecutter.project_slug }}",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


def trigger_error(request):
    return 1 / 0


router = routers.DefaultRouter()
router.registry.extend(common_router.registry)
router.registry.extend(event_card_router.registry)
urlpatterns = [
    path('', include('{{ cookiecutter.project_slug }}.api')),
    path('admin/', admin.site.urls),
    path('sentry-debug/', trigger_error),
    path('accounts/profile/', ProfileView.as_view()),
    url(r'api/swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'api/redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    url(r'api/swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
handler500 = 'rest_framework.exceptions.server_error'
handler400 = 'rest_framework.exceptions.bad_request'
