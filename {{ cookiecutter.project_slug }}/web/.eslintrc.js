module.exports = {
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  plugins: ['babel', 'import', 'jsx-a11y', 'react', 'prettier', 'sonarjs'],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    'airbnb',
    'plugin:sonarjs/recommended'],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
    "project": "tsconfig.json",
    "tsconfigRootDir": "."
  },
  rules: {
    '@typescript-eslint/explicit-function-return-type': 0,
    'import/no-extraneous-dependencies': 0,
    'import/prefer-default-export': 0,
    'import/no-cycle': 'warn',
    'import/no-mutable-exports': 'warn',
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'no-unused-vars': ['error', { vars: 'all', args: 'after-used', ignoreRestSiblings: false }],
    'linebreak-style': 'off', // Неправильно работает в Windows.
    'implicit-arrow-linebreak': 'off',
    'arrow-parens': 'off', // Несовместимо с prettier
    'object-curly-newline': 'off', // Несовместимо с prettier
    'no-mixed-operators': 'off', // Несовместимо с prettier
    'arrow-body-style': 'off', // Это - не наш стиль?
    'function-paren-newline': 'off', // Несовместимо с prettier
    'no-plusplus': 'off',
    'space-before-function-paren': 0, // Несовместимо с prettier
    'no-prototype-builtins': 0,
    'no-underscore-dangle': 0,
    'class-methods-use-this': 0,
    indent: 0,
    'no-continue': 0,
    'operator-linebreak': 0,
    'max-classes-per-file': 0,
    'consistent-return': 0,
    'no-bitwise': 0,
    'no-unused-expressions': 'warn',
    'prefer-const': 'warn',
    camelcase: 'warn',
    'no-useless-escape': 'warn',
    'no-restricted-syntax': 'warn',
    'no-confusing-arrow': 'warn',
    'no-return-assign': 'warn',
    eqeqeq: 'warn',
    'dot-notation': 'warn',
    'array-callback-return': 'warn',
    'no-use-before-define': 'warn',
    'guard-for-in': 'warn',
    'no-shadow': 'warn',
    'prefer-template': 'warn',
    'jsx-a11y/media-has-caption': 'warn',
    'no-nested-ternary': 'warn',
    'new-cap': 'warn',
    'no-void': 'warn',
    'no-useless-return': 'warn',
    strict: 'warn',
    'quotes': 'warn',
    'prefer-object-spread': 'warn',

    'max-len': ['error', 120, 2, { ignoreUrls: true }], // airbnb позволяет некоторые пограничные случаи
    'no-console': 'warn', // airbnb использует предупреждение
    'no-alert': 'error', // airbnb использует предупреждение

    'no-param-reassign': 'off', // Это - не наш стиль?
    radix: 'off', // parseInt, parseFloat и radix выключены. Мне это не нравится.

    'react/static-property-placement': 0,
    'react/forbid-prop-types': 0,
    'react/require-default-props': 'warn',

    'prefer-destructuring': 'off',

    'react/no-find-dom-node': 'off', // Я этого не знаю
    'react/no-did-mount-set-state': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'react/no-this-in-sfc': 'warn',
    'react/prop-types': 0,
    'react/jsx-boolean-value': 0,
    'react/destructuring-assignment': 0,
    'react/jsx-filename-extension': 0,
    'react/jsx-curly-newline': 0,
    'react/no-array-index-key': 0,
    'react/jsx-wrap-multilines': 0,
    'react/prefer-stateless-function': 0,

    'jsx-a11y/label-has-associated-control': 0,
    'jsx-a11y/label-has-for': 0,
    'jsx-a11y/anchor-is-valid': ['error', { components: ['Link'], specialLink: ['to'] }],

    'prettier/prettier': ['error'],
  },
  overrides: [
    {
      files: ["*.d.ts"],
      rules: {
        "no-empty-pattern": "off"
      }
    }
  ],
};
